package Type;

public enum StatType {
	LIFE,		// Pdv
	MANA,		// Quantite de mana
	INTEL,		// Intel
	STRENGTH,	// Force
	STAMINA,	// Endurance
	STEALTH,	// Discretion
	NOTSET
}
