package Type;

public enum Element {
	NEUTRAL,
	FIRE,
	WATER,
	EARTH,
	WIND,
	LIGHT,
	DARK,
	BLOOD
}