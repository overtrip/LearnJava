package Type;

public enum ClassList {
	NOOB,		// Default Class
	// Main class
	WIZARD,		// Mage
	WARRIOR,	// Guerrier
	ARCHER,		// Archer
	ROGUE,		// Assassin
	PALADIN,	// Paladin
	MONK,		// Moine
	FENCER,		// Breteur
	NECRO,		// Necromancien
	TAMER,		// Dresseur

	// Sub class
	FARMER,     // Fermier
	ALCHEMIST,	// Alchimiste
	SCRIBE,		// Scribe
	FORGER,		// Forgeur
	CARVER,		// Sculpteur
	MINER,		// Mineur
	COMMANDER,	// Commandant
	NOTSET
}
