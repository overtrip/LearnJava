package Type;

public enum StuffPosition {
	HEAD,
	NECK,
	WRISTLEFT,
	WRISTRIGTH,
	TORC,
	CLOAK,
	LEGS,
	WEAPONLEFT,
	WEAPONRIGTH
}
