package Type;

public enum LearnType {
	STAT,
	SPELL,
	CLASS,
	NOTSET
}