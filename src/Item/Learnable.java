package Item;

import SpellList.Spell;
import Type.LearnType;
import Type.StatType;

//	Item type who get stats
public class Learnable {
	private final static int	defaultValue	= 1;
	private String				name;
	private int 				statValue 		= 1;
	private StatType			statType 		= StatType.NOTSET;
	private LearnType			learnType		= LearnType.NOTSET;
	private Spell				spell;

	// Spell Constructor
	public Learnable(String name, LearnType learnType, Spell spell) {
		this.setName(name);
		if (learnType == LearnType.SPELL) {
			this.setLearnType(learnType);
			this.setSpell(spell);
		}
	}

	// Stat constructor Genreic
	public Learnable(String name, LearnType learnType, StatType type) {
		this(name, learnType, type, defaultValue);
	}

	// Stat constructor with specific value
	public Learnable(String name, LearnType learnType, StatType type, int statValue) {
		this.setName(name);
		if (learnType == LearnType.STAT) {
			this.setStatType(type);
			this.setStatValue(statValue);
		}
		this.setLearnType(learnType);
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getStatValue() {
		return statValue;
	}

	public void setStatValue(int statValue) {
		this.statValue = statValue;
	}

	public StatType getStatType() {
		return statType;
	}

	public void setStatType(StatType statType) {
		this.statType = statType;
	}

	public LearnType getLearnType() {
		return learnType;
	}

	public void setLearnType(LearnType learnType) {
		this.learnType = learnType;
	}

	public Spell getSpell() {
		return spell;
	}

	public int getInvestCost() {
		int result = 0;
		
		if (this.getLearnType() == LearnType.SPELL) {
			result += this.CostForSpell() + this.CostForStat();
		}
		else if (this.learnType == LearnType.STAT) {
			result += this.CostForStat();
		}
		return (result);
	}

	public int CostForSpell() {
		return (this.spell.getCostLearning());
	}

	public int CostForStat() {
		return (this.getStatValue());
	}

	public void setSpell(Spell spell) {
		this.spell = spell;
	}

	public void printType() {
		System.out.println("Le type est : " + this.learnType.name());
	}

	public void printStatType() {
		System.out.println("La stat est : " + this.statType.name());
	}

	public void printName() {
		System.out.println("Le nom de l'objet est : " + this.getName());
	}
	public void printSpell() {
		if (this.getLearnType() == LearnType.SPELL) {
			System.out.println("Le nom du sort est : " + this.getSpell().getName());
		}
		else {
			System.out.println(this.getName() + "n'est pas un spell");
		}
	}
}
