package SpellList;

import Type.ClassList;
import Type.Element;
import Type.SpellList;

public class Hit extends Spell {

	public Hit() {
		// Type / Power / Cooldown / Range / AOE / Cost / Name / Damage / ClassUse / CostLearn
		super(Element.NEUTRAL, 1, 10, 1, 1, 1, SpellList.HIT.name(), 0, ClassList.NOOB, 0);
	}
}