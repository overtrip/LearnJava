package SpellList;

import Perso.UserPerso;
import Type.ClassList;
import Type.Element;
import Type.StatType;
import Type.SpellList;

public class Flammiche extends Spell {

	public Flammiche() {
		// Type / Power / Cooldown / Range / AOE / Cost / Name / Damage / ClassUse / CostLearn
		super(Element.FIRE, 1, 10, 1, 1, 1, SpellList.FLAMMICHE.name(), 0, ClassList.WIZARD, 10);
	}

	public boolean condition(UserPerso perso) {
		boolean result = false;
		if (perso.containSpell(SpellList.HIT.name())) {
			if (perso.getStat(StatType.MANA) >= 1) {
				result = true;
				System.out.println("Les conditions pour apprendre " + this.getName() + " sont remplis");
			}
			else {
				result = true;
				System.out.println("Pas assez de point dans le sort");
			}
		}
		else {
			System.out.println("Vous ne pocedez pas les pre requis pour apprendre le sort :" + this.getName());
		}
		return result;
	}
}
