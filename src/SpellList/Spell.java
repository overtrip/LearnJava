package SpellList;

import Type.Element;
import Type.ClassList;
import Perso.UserPerso;

public class Spell {
	private Element		element;					// Element Type
	private ClassList	witchClass;					// Witch class use this spell
	private int			power 			= 0;		// Power
	private	int		 	coolDown		= 100;		// Time before launch again
	private int		 	range			= 1;		// Witch length launch spell
	private int		 	rangeSize		= 1;		// AOE of spell
	private int		 	cost			= 1;		// Who mana cost
	private String	 	name;						// Name of spell
	private int		 	investPoint 	= 0;		// How much spell has been used
	private int		 	damage			= 0;		// Nb of damage
	private	boolean	 	unlock			= false;	// Spell not yet unlock
	private int			costLearning	= 0;		// How mutch spell cost to learn

	public Spell(Element element, int power, int coolDown, int range, int rangeSize, int cost, String name, int damage, ClassList whichClass, int costLearning) {
		this.setElement(element);
		this.setPower(power);
		this.setCoolDown(coolDown);
		this.setRange(range);
		this.setRangeSize(rangeSize);
		this.setCost(cost);
		this.setName(name);
		this.setDamage(damage);
		this.setWitchClass(whichClass);
		this.setCostLearning(costLearning);
	}

	// Begin of Initialisation Stats Section
	public void setName(String name) {
		this.name = name;
	}

	public void setElement(Element element) {
		this.element = element;
	}

	public void setPower(int power) {
		this.power = power;
	}

	public void setCoolDown(int coolDown) {
		this.coolDown = coolDown;
	}

	public void setRange(int range) {
		this.range = range;
	}

	public void setRangeSize(int rangeSize) {
		this.rangeSize = rangeSize;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public void setInvestPoint(int investPoint) {
		this.investPoint = investPoint;
	}

	public void setDamage(int damage) {
		this.damage = damage;
	}

	public void setWitchClass(ClassList witchClass) {
		this.witchClass = witchClass;
	}

	public boolean condition(UserPerso user) {
		return true;
	}

	@Override
	public boolean equals(Object obj) {
		Spell tmp;
		
		if (obj instanceof Spell) {
			tmp = (Spell)obj;
		    return tmp.getName() == this.getName();
		}
	    return false;
	}
	
	public void setCostLearning(int costLearning) {
		this.costLearning = costLearning;
	}
	// END of Initialisation Stats Section

	// Begin of Geter Stats Sectio
	public Element getElement() {
		return this.element;
	}

	public int getPower() {
		return this.power;
	}

	public int getCoolDown() {
		return this.coolDown;
	}

	public int getRange() {
		return this.range;
	}

	public int getRangeSize() {
		return this.rangeSize;
	}

	public int getCost() {
		return this.cost;
	}

	public String getName() {
		return this.name;
	}

	public int getInvestPoint() {
		return this.investPoint;
	}

	public int getDamage() {
		return (this.damage);
	}

	public boolean isUnlock() {
		return unlock;
	}

	public void setUnlock() {
		this.unlock = true;
	}

	public ClassList getWitchClass() {
		return this.witchClass;
	}

	public int getCostLearning() {
		return this.costLearning;
	}
	// End of Geter Stats Sectio

	// Begin of Upgrade Stats Section
	public void UpPower(int upPower) {
		this.setPower(this.getPower() + upPower);
	}

	public void DownCoolDown(int reduc) {
		this.setCoolDown(this.getCoolDown() - reduc);
	}

	public void UpRange(int upRange) {
		this.setRange(this.getRange() + upRange);
	}

	public void UpRangeSize(int upRangeSize) {
		this.setRangeSize(this.getRangeSize() + upRangeSize);
	}

	public void DownCost(int costLess) {
		this.setCoolDown(this.getCoolDown() + costLess);
	}

	public void UpSpell(int investPoint) {
		this.setInvestPoint(this.getInvestPoint() + investPoint);
	}
	// End of Upgrade Stats Section
}
