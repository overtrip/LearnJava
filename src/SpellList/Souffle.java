package SpellList;

import Type.Element;
import Type.ClassList;

public class Souffle extends Spell {

	public Souffle() {
		// Type / Power / Cooldown / Range / AOE / Cost / Name / Damage / ClassUse / CostLearn
		super(Element.NEUTRAL, 1, 10, 1, 1, 1, "Souffle", 0, ClassList.NOOB, 1);
	}
}
