package Class;

import Type.ClassList;

public class Class {
	private ClassList	name;
	private int			investPoint		= 0;
	private	boolean		unlock			= false;
	private int			CostLearning	= 0;

	public Class(ClassList name) {
		this.setName(name);
	}

	public ClassList getName() {
		return this.name;
	}

	public void setName(ClassList name) {
		this.name = name;
	}

	public int getInvestPoint() {
		return investPoint;
	}

	public void setInvestPoint(int investPoint) {
		this.investPoint = investPoint;
	}

	public boolean isUnlock() {
		return unlock;
	}

	public void setUnlock() {
		this.unlock = true;
	}

	public int getCostLearning() {
		return this.CostLearning;
	}

	public void setCostLearning(int costLearning) {
		CostLearning = costLearning;
	}
}
