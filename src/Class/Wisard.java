package Class;

import Type.ClassList;

public class Wisard extends Class {

	private ClassList	name;
	private int			CostClass = 666;

	public  Wisard() {
		super(ClassList.WIZARD);
		this.setName(ClassList.WIZARD);
		this.setCostLearning(this.CostClass);
	}

	public ClassList getName() {
		return this.name;
	}

	public void setName(ClassList name) {
		this.name = name;
	}

	public int getCostLearning() {
		return this.CostClass;
	}
}
