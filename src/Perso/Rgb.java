package Perso;

public class Rgb {
	private int	red;
	private int	green;
	private int	blue;

	public Rgb(int r, int g, int b) {
		setRed(r);
		setGreen(g);
		setBlue(b);
	}

	public Rgb() {
		setRed('\0');
		setGreen('\0');
		setBlue('\0');
	}

	public Rgb copyColor() {
		return new Rgb(this.getRed(), this.getGreen(), this.getBlue());
	}

	public int getRed() {
		return red;
	}

	public void setRed(int red) {
		this.red = red;
	}

	public int getGreen() {
		return green;
	}

	public void setGreen(int green) {
		this.green = green;
	}

	public int getBlue() {
		return blue;
	}

	public void setBlue(int blue) {
		this.blue = blue;
	}

	public void printColor() {
		System.out.println("red:\t" + getRed() + "\ngreen:\t" +  getGreen() + "\nblue:\t" + getBlue() + "\n");
	}

	public void modifyColor(Rgb toModify) {
		if (getRed() != toModify.getRed()) {
			setRed(toModify.getRed());
		}
		if (getGreen() != toModify.getGreen()) {
			setGreen(toModify.getGreen());
		}
		if (getBlue() != toModify.getBlue()) {
			setBlue(toModify.getBlue());
		}
	}
}