package Perso;

import Type.*;
import SpellList.*;
import Item.Learnable;


public class Main {

	public static void main(String[] args) {
									// Type / Power / Cooldown / Range / AOE / Cost / Name / Damage
		Flammiche	flammiche	= new Flammiche();
		Hit			hit			= new Hit();
		Learnable	bookspell1	= new Learnable(LearnItemList.BOOK.name(), LearnType.SPELL, flammiche);
		Learnable	bookspell2	= new Learnable(LearnItemList.BOOK.name(), LearnType.SPELL, hit);
//		Learnable	bookspell3	= new Learnable(LearnItemList.BOOK.name(), LearnType.STAT, StatType.LIFE, 2);
		Learnable	bookspell4	= new Learnable(LearnItemList.BOOK.name(), LearnType.STAT, StatType.MANA);
		UserPerso	Perso		= new UserPerso();

		Perso.useItem(bookspell4);
		Perso.useItem(bookspell2);
		Perso.useItem(bookspell1);

	}

}
