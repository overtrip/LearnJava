package Perso;
import java.util.HashMap;

import Type.StatType;

public class Stats {
	private	HashMap<StatType, Integer> stat = new HashMap<>();

	public Stats() {
		this.stat.put(StatType.LIFE, 10);
		this.stat.put(StatType.INTEL, 0);
		this.stat.put(StatType.MANA, 0);
		this.stat.put(StatType.STRENGTH, 1);
		this.stat.put(StatType.STAMINA, 10);
		this.stat.put(StatType.STEALTH, 0);
	}

	public void increaseStat(StatType type, int nb) {
		int oldStat = this.stat.get(type);
		this.stat.replace(type, oldStat + nb);
	}

	public void printStat(StatType type) {
		System.out.println("La stat " + type.name() + " est de : " + this.stat.get(type));
	}

	public void printStats() {
		for (StatType key : StatType.values()) {
			System.out.println(key + " - " + this.stat.get(key));
		}
	}

	public int getStat(StatType stat) {
			return (this.stat.get(stat));
	}
}
