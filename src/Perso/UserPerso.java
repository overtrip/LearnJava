package Perso;

import java.util.ArrayList;
import Item.Learnable;
import SpellList.Spell;
import Class.Class;
import Type.Element;
import Type.LearnType;
import Type.StatType;
import Type.ClassList;

public class UserPerso {
	private int lifeStat = 100000; // Golable points to invaste in perso
	private String name = "Bob"; // Name of perso
	private Motion trais; // Graphic parameter of perso
	private ArrayList<Class> classList = new ArrayList<>(); // List of unlocked class
	private ArrayList<Spell> spells = new ArrayList<>(); // List of unlocked spells
	private Stats stats = new Stats(); // Stats of personnage invest by points

	public UserPerso(String name, Motion trais) {
		this.setName(name);
		this.setTrais(trais);
		this.spells = new ArrayList<>();
		this.classList = new ArrayList<>();
	}

	public UserPerso() {

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Motion getTrais() {
		return trais;
	}

	public void setTrais(Motion trais) {
		this.trais = trais;
	}

	public int getLifeStat() {
		return lifeStat;
	}

	public void setLifeStat(int lifeStat) {
		this.lifeStat = lifeStat;
	}

	public ArrayList<Class> getClassList() {
		return classList;
	}

	public void setClassList(ArrayList<Class> classList) {
		this.classList = classList;
	}

	public Stats getStats() {
		return stats;
	}

	public int getStat(StatType stat) {
		return (this.stats.getStat(stat));
	}

	public boolean useItem(Learnable item) {
		boolean result = false;

		if (item.getLearnType() == LearnType.STAT) {
			// if item is Stat type so get the Type and increase by current value
			this.stats.increaseStat(item.getStatType(), item.getStatValue());
			result = true;
			System.out.println("L'item : " + item.getName() + " vous a augmente la stat " + item.getStatType()
					+ " de : " + item.getStatValue());
			this.stats.getStat(item.getStatType());
		} else if (item.getLearnType() == LearnType.SPELL) {
			this.addSpell(item.getSpell());
			result = true;
		}
		this.setLifeStat(this.getLifeStat() - this.investCost(item));
		System.out.println("il reste " + this.getLifeStat());
		return result;
	}

	public void addClass(ClassList NewClass) {
		this.classList.add(new Class(NewClass));

	}

	public void addSpell(Spell spell) {
		if (this.spells.contains(spell)) {
			System.out.println(spell.getName() + "  est deja appris ");
		} else {
			if (spell.condition(this)) {
				this.spells.add(spell);
				this.getSpell(spell).setUnlock();
				System.out.println("Vous avez appris le sort \"" + spell.getName() + "\" de type: " + spell.getElement());
				if (!this.getClassList().contains(spell.getWitchClass())) {
					this.addClass(spell.getWitchClass());
					System.out.println("Vous avez debloque la classe " + spell.getWitchClass());
				}
			} else {
				System.out.println("Vous n'avez pas appris le sort");
			}
		}
	}

	public int investCost(Learnable item) {
		int result = item.getInvestCost();

		
		/* TODO ---------------------------------------------------------------
		-
		- Find solution to get cost of new class learn with a learnable object
		---------------------------------------------------------------------*/
//		if (item.getLearnType() == LearnType.SPELL) {
//			if (!this.getClassList().contains(item.getSpell().getWitchClass())) {
//				result += item.getSpell().getCostLearning();
//				System.out.println("ca coute " + item.getSpell().getCostLearning());
//			}
//		}
		return (result);
	}

	public void setSpells(ArrayList<Spell> spells) {
		this.spells = spells;
	}

	public int getIndexOfSpell(Spell spell) {
		return (this.spells.indexOf(spell));
	}

	public Spell getSpell(Spell spell) {
		return (this.spells.get(this.getIndexOfSpell(spell)));
	}

	public ArrayList<Spell> getSpells() {
		return this.spells;
	}

	public boolean containSpell(String spellName) {
		boolean result = false;

		for (Spell s : spells) {
			if (s.getName() == spellName) {
				result = true;
				break;
			}
		}
		return result;
	}

	public void printStat(StatType stat) {
		// Print one specific stat
		this.stats.printStat(stat);
	}

	public void printStats() {
		// Print all stats
		this.stats.printStats();
	}
}
