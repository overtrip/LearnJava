package Perso;

public class Motion {
	public Rgb	color;
	public int	size;
	public int	corp;
	
	public Motion(Rgb color, int size, int corp) {
		this.setColor(color);
		this.setSize(size);
		this.setCorp(corp);
	}
	public Motion() {
		this.setColor(new Rgb());
		this.setSize(0);
		this.setCorp(0);
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public int getCorp() {
		return corp;
	}

	public void setCorp(int corp) {
		this.corp = corp;
	}

	public Rgb getColor() {
		return color;
	}

	public void setColor(Rgb color) {
		this.color = color;
	}
}
